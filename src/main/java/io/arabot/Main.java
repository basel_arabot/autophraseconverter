package io.arabot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * Created by basil on 8/21/17.
 */
public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException{
        if (args.length <= 0) {
            logger.error("Invalid arguments, you must specify input and output folder name");
            logger.error("Usage: java -jar autophraseConverter.jar inputFileName.txt outputFileName.txt");
            System.exit(1);
        }
        String formattedLines = convert(args[0]);
        writeToFile(args[1], formattedLines);
    }

    public static String convert(String fileName) throws IOException{
        StringBuilder formattedlines = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                formattedlines.append(line + "=>" + line.replace(" ", "_"));
                formattedlines.append(System.lineSeparator());
            }
        }
        return formattedlines.toString();
    }

    public static void writeToFile(String outputFileName, String formattedlines) {
        BufferedWriter bw = null;
        FileWriter fw = null;
        try {
            fw = new FileWriter(outputFileName);
            bw = new BufferedWriter(fw);
            bw.write(formattedlines);
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
        }
    }

}
